-- MY SMART KEY CHAIN --

This project was created for the WGU Capstone project by Francisco J. Martin de Blas

My Smart Key Chain is the the first of two deliverables that are part of the whole solution for a new Electronic Access Control (EAC) system

-- Instalation of the application

Android apps are packaged on .apk file, you can find the last built and sign .apk file in the path

https://gitlab.com/franjmartin21/MySmartKeyChain/blob/master/app/staging/app-staging.apk

STEPS:

1- Download the file into your device

2- You will need to enable the option "Install from unknown sources". You should be able to see this setting by navigating to Setting > Security.

-- sometimes the device gives you the option when trying to install the app

3- Install the application by finding it in the your file explorer, navigate to the downloads folder, and click into the .apk downloaded

