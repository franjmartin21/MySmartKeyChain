package fmart43.com.mysmartkeychain.fingerprint

import android.app.KeyguardManager
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import android.security.keystore.KeyProperties
import android.view.inputmethod.InputMethodManager
import android.security.keystore.KeyGenParameterSpec
import android.util.Base64
import timber.log.Timber
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import java.security.spec.ECGenParameterSpec
import java.security.spec.InvalidKeySpecException
import java.security.spec.MGF1ParameterSpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource

const val MYSMARTKEYCHAIN_KEYNAME = "mysmartkeychain"

const val KEY_STORE = "AndroidKeyStore"

class FingerprintAPI private constructor() {

    lateinit var fingerprintManager: FingerprintManager

    lateinit var keyguardManager: KeyguardManager

    lateinit var keyStore: KeyStore

    lateinit var cipher: Cipher

    companion object {
        private var INSTANCE: FingerprintAPI? = null

        fun getInstance(context: Context): FingerprintAPI =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: initializeFingerprintApi(context).also { INSTANCE = it }
                }

        fun initializeFingerprintApi(context: Context): FingerprintAPI {
            val fingerprintAPI = FingerprintAPI()
            fingerprintAPI.fingerprintManager = context.getSystemService(FingerprintManager::class.java)
            val keyguardManager = context.getSystemService(KeyguardManager::class.java)
            fingerprintAPI.keyguardManager = keyguardManager
            val keyStore = initKeyStore()
            if (!keyStore.containsAlias(MYSMARTKEYCHAIN_KEYNAME)) generateNewKey()
            fingerprintAPI.keyStore = keyStore
            fingerprintAPI.cipher = initCipher()
            return fingerprintAPI
        }

        private fun initKeyStore(): KeyStore {
            val keyStore = KeyStore.getInstance(KEY_STORE)
            keyStore.load(null)
            return keyStore
        }

        private fun initCipher(): Cipher {
            return Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")
        }

        private fun generateNewKey() {
            val sKeyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEY_STORE)
            try {
                sKeyPairGenerator.initialize(KeyGenParameterSpec.Builder(MYSMARTKEYCHAIN_KEYNAME,
                        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                        .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                        .setUserAuthenticationRequired(true)
                        .build())
                sKeyPairGenerator.generateKeyPair()
            }catch (e: Throwable){
                Timber.e(e.message)
            }
        }
    }

    fun checkForSupportedHardware(): Boolean {
        return fingerprintManager.isHardwareDetected
    }

    fun checkForEnrolledFingerPrints(): Boolean {
        return fingerprintManager.hasEnrolledFingerprints()
    }

    fun checkForKeyguardSecured(): Boolean {
        return keyguardManager.isKeyguardSecure
    }

    private fun initCipherMode(mode: Int): Boolean {
        try {
            keyStore.load(null)
            when (mode) {
                Cipher.ENCRYPT_MODE -> {
                    val key = keyStore.getCertificate(MYSMARTKEYCHAIN_KEYNAME).getPublicKey()
                    val unrestricted = KeyFactory.getInstance(key.getAlgorithm()).generatePublic(X509EncodedKeySpec(key.getEncoded()))
                    val spec = OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT)
                    cipher.init(mode, unrestricted, spec)
                }

                Cipher.DECRYPT_MODE -> {
                    val privateKey = keyStore.getKey(MYSMARTKEYCHAIN_KEYNAME, null) as PrivateKey
                    cipher.init(mode, privateKey)
                }
                else -> return false
            }
            return true
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: CertificateException) {
            e.printStackTrace()
        } catch (e: UnrecoverableKeyException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeySpecException) {
            e.printStackTrace()
        }

        return false
    }

    fun encryptString(string: String): String? {
        initCipherMode(Cipher.ENCRYPT_MODE)
        val bytes = cipher.doFinal(string.toByteArray())
        return Base64.encodeToString(bytes, Base64.NO_WRAP)
        return null
    }

    fun getCryptoObject(): FingerprintManager.CryptoObject? {
        return if (initCipherMode(Cipher.DECRYPT_MODE)) {
            FingerprintManager.CryptoObject(cipher)
        } else null
    }

}