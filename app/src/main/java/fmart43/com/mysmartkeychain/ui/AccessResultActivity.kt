package fmart43.com.mysmartkeychain.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import fmart43.com.mysmartkeychain.R
import kotlinx.android.synthetic.main.activity_access_result.*


class AccessResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_result)
        val result = intent.getStringExtra("result")
        if(result == "ACCESS AUTHORIZED")
            layout.setBackgroundColor(Color.parseColor("#388E3C"))
        else
            layout.setBackgroundColor(Color.parseColor("#D32F2F"))
        val message = intent.getStringExtra("errorMessage")
        tv_result.text = result
        tv_message.text = message
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // do something on back.
            startActivity(Intent(this, KeyListActivity::class.java))
        }
        return super.onKeyDown(keyCode, event)
    }
}
