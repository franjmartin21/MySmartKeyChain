package fmart43.com.mysmartkeychain.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.hardware.fingerprint.FingerprintManager
import android.support.design.widget.Snackbar
import android.view.View
import fmart43.com.mysmartkeychain.exception.KeyException
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.repository.KeyRepository
import fmart43.com.mysmartkeychain.ui.AddKeyActivity
import fmart43.com.mysmartkeychain.ui.viewmodel.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class KeyListViewModel(private val keyRepository: KeyRepository) : BaseViewModel() {

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()

    val errorClickListener = View.OnClickListener { loadKeys() }

    val keyList: MutableLiveData<List<Key>> = MutableLiveData()

    init {
        loadKeys()
    }

    fun loadKeys() {
        subscription = Observable.fromCallable { keyRepository.getAllRegisteredKeys() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveKeyListStart() }
                .doOnTerminate { onRetrieveKeyListFinish() }
                .subscribe(
                        { result -> onRetrieveKeyListSuccess(result) },
                        { onRetrieveKeyListError(it) }

                )
    }

    private fun onRetrieveKeyListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveKeyListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveKeyListSuccess(keyListReturned: List<Key>) {
        keyList.value = keyListReturned
    }

    private fun onRetrieveKeyListError(e: Throwable) {
        if (e is KeyException)
            errorMessage.value = e.resourceStrId
    }

    fun onClick(view: View) {
        val intent = Intent(view.context, AddKeyActivity::class.java)
        view.context.startActivity(intent)
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}