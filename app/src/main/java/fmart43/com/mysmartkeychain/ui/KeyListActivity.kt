package fmart43.com.mysmartkeychain.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.hardware.fingerprint.FingerprintManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.databinding.ActivityKeyListBinding
import fmart43.com.mysmartkeychain.injection.ViewModelFactory
import fmart43.com.mysmartkeychain.ui.adapter.KeyListAdapter
import fmart43.com.mysmartkeychain.ui.handler.FingerprintHandler
import fmart43.com.mysmartkeychain.ui.listener.IAuthenticateListener
import fmart43.com.mysmartkeychain.ui.viewmodel.KeyListViewModel
import kotlinx.android.synthetic.main.activity_key_list.*
import timber.log.Timber

class KeyListActivity : AppCompatActivity(), IAuthenticateListener {

    private lateinit var binding: ActivityKeyListBinding
    private lateinit var viewModel: KeyListViewModel

    private var errorSnackbar: Snackbar? = null

    val keyListAdapter: KeyListAdapter = KeyListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        Timber.d("Accessing...")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_key_list)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(KeyListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
        viewModel.keyList.observe(this, Observer {
            it?.let { keyListAdapter.updateKeyList(it) }
        })

        binding.viewModel = viewModel
        binding.keyList.adapter = keyListAdapter
        binding.keyList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_key_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAuthenticate(decryptPassword: String) {
        Toast.makeText(this, "Fingerpring Authenticated $decryptPassword", Toast.LENGTH_LONG).show()
    }
}
