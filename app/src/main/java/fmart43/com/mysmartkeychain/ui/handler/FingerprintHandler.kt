package fmart43.com.mysmartkeychain.ui.handler

import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import android.util.Base64
import android.widget.Toast
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.ui.listener.IAuthenticateListener
import javax.crypto.Cipher

internal class FingerprintHandler(private val mContext: Context, private val message: String, private val mListener: IAuthenticateListener) : FingerprintManager.AuthenticationCallback() {
    private val mCancellationSignal: CancellationSignal?

    init {
        mCancellationSignal = CancellationSignal()
    }

    fun startAuth(fingerprintManager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        fingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0, this, null)
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
        //Toast.makeText(mContext, errString, Toast.LENGTH_SHORT).show()
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
        //Toast.makeText(mContext, helpString, Toast.LENGTH_SHORT).show()
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
        val cipher = result.cryptoObject.cipher
        val decoded =  decryptString(message, cipher)
        mListener.onAuthenticate(decoded.toString())
    }

    fun decryptString(string: String, cipher: Cipher): String? {
        val bytes = Base64.decode(string, Base64.NO_WRAP)
        return String(cipher.doFinal(bytes))
    }

    override fun onAuthenticationFailed() {
        Toast.makeText(mContext, R.string.fingerpring_incorrect, Toast.LENGTH_SHORT).show()
    }

    fun cancel() {
        mCancellationSignal?.cancel()
    }
}
