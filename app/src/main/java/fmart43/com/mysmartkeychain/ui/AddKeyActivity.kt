package fmart43.com.mysmartkeychain.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.databinding.ActivityAddKeyBinding
import fmart43.com.mysmartkeychain.injection.ViewModelFactory
import fmart43.com.mysmartkeychain.ui.viewmodel.AddKeyViewModel

class AddKeyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddKeyBinding

    private lateinit var viewModel: AddKeyViewModel

    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_key)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(AddKeyViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })

        viewModel.registrationCompleted.observe(this, Observer {
            Toast.makeText(this, "Your get a new Key registered, ready to use!", Toast.LENGTH_LONG).show()
            val intent = Intent(this, KeyListActivity::class.java)
            startActivity(intent)
        })

        binding.viewModel = viewModel

    }


    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }
}
