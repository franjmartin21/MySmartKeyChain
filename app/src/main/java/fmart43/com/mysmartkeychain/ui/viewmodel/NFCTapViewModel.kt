package fmart43.com.mysmartkeychain.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.hardware.fingerprint.FingerprintManager
import android.view.View
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.exception.KeyException
import fmart43.com.mysmartkeychain.network.dto.InitializeUserDoorAuthorizationResponseDTO
import fmart43.com.mysmartkeychain.repository.KeyRepository
import fmart43.com.mysmartkeychain.ui.viewmodel.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class NFCTapViewModel(val keyRepository: KeyRepository) : BaseViewModel() {

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val dataFieldsVisibility: MutableLiveData<Int> = MutableLiveData()

    val authorizationStatus: MutableLiveData<Boolean> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()

    val organizationName: MutableLiveData<String> = MutableLiveData()

    val password: MutableLiveData<String> = MutableLiveData()

    val doorName: MutableLiveData<String> = MutableLiveData()

    val username: MutableLiveData<String> = MutableLiveData()

    var fingerprintPass: String? = null

    var fingerprintPassEnabled: MutableLiveData<Int> = MutableLiveData()

    val url: MutableLiveData<String> = MutableLiveData()

    lateinit var nfc: String

    lateinit var doorId: String

    fun initialize(nfc: String, doorId: String) {
        this.nfc = nfc
        this.doorId = doorId
        subscription = Observable.fromCallable {
            keyRepository.initAuthorizationOnDoor(nfc, doorId)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onAuthorizationOnDoorStart() }
                .doOnTerminate { onAuthorizationOnDoorFinished() }
                .subscribe(
                        { result -> onAuthorizationOnDoorSuccess(result) },
                        { error -> onAuthorizationOnDoorError(error) }

                )
    }

    private fun onAuthorizationOnDoorStart() {
        loadingVisibility.value = View.VISIBLE
        dataFieldsVisibility.value = View.GONE
        fingerprintPassEnabled.value = View.GONE
        errorMessage.value = null
    }

    private fun onAuthorizationOnDoorFinished() {
        loadingVisibility.value = View.GONE
        dataFieldsVisibility.value = View.VISIBLE
    }

    private fun onAuthorizationOnDoorSuccess(result: InitializeUserDoorAuthorizationResponseDTO?) {
        result?.let {
            organizationName.value = it.nameCompany
            doorName.value = it.doorName
            username.value = it.userName
            fingerprintPass = it.fingerprintPass
            fingerprintPassEnabled.value = if (!it.fingerprintPass.isNullOrEmpty() && keyRepository.isFingerprintAvailable()) View.VISIBLE else View.GONE
        }
    }

    private fun onAuthorizationOnDoorError(e: Throwable) {
        if (e is KeyException)
            errorMessage.value = e.resourceStrId
        else
            errorMessage.value = R.string.unknown_error
        authorizationStatus.value = false

    }

    fun accessClick() {
        subscription = Observable.fromCallable {
            keyRepository.completeAuthorizationOnDoor(nfc, doorId, password.value.toString())
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onCompleteAuthorizationStart() }
                .doOnTerminate { onCompleteAuthorizationFinished() }
                .subscribe(
                        { result -> onCompleteAuthorizationSuccess(result) },
                        { error -> onCompleteAuthorizationError(error) }

                )
    }

    private fun onCompleteAuthorizationStart() {
        loadingVisibility.value = View.VISIBLE
        dataFieldsVisibility.value = View.GONE
        fingerprintPassEnabled.value = View.GONE
        errorMessage.value = null
    }

    private fun onCompleteAuthorizationFinished() {
        loadingVisibility.value = View.GONE
        dataFieldsVisibility.value = View.VISIBLE
    }

    private fun onCompleteAuthorizationSuccess(authorized: Boolean) {
        authorizationStatus.value = authorized
    }

    private fun onCompleteAuthorizationError(e: Throwable) {
        if (e is KeyException)
            errorMessage.value = e.resourceStrId
        authorizationStatus.value = false
    }

    fun findCryptoObjectForFingerprint(): FingerprintManager.CryptoObject = keyRepository.getCryptoObjectForFingerprint()!!

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

}
