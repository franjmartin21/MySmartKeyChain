package fmart43.com.mysmartkeychain.ui.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.databinding.ItemKeyBinding
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.ui.viewmodel.KeyViewModel

class KeyListAdapter : RecyclerView.Adapter<KeyListAdapter.ViewHolder>() {

    private lateinit var keyList: List<Key>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemKeyBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_key, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (::keyList.isInitialized) keyList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(keyList[position])
    }

    fun updateKeyList(keyList: List<Key>) {
        this.keyList = keyList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemKeyBinding) : RecyclerView.ViewHolder(binding.root) {

        private val viewModel = KeyViewModel()

        fun bind(key: Key) {
            viewModel.bind(key)
            binding.viewModel = viewModel
        }
    }
}