package fmart43.com.mysmartkeychain.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.ui.viewmodel.base.BaseViewModel

class KeyViewModel : BaseViewModel() {

    private val organization = MutableLiveData<String>()

    private val userName = MutableLiveData<String>()

    fun bind(key: Key) {
        organization.value = key.organization
        userName.value = key.userName
    }

    fun getOrganization(): MutableLiveData<String> {
        return organization
    }

    fun getUsername(): MutableLiveData<String> {
        return userName
    }
}