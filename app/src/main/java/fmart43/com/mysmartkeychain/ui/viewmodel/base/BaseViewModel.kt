package fmart43.com.mysmartkeychain.ui.viewmodel.base

import android.arch.lifecycle.ViewModel
import android.net.Uri

abstract class BaseViewModel : ViewModel() {
    protected fun isValidUrl(url: Uri): Boolean {
        return if (url.scheme.isNullOrBlank()) false
        else if (url.authority.isNullOrBlank()) false
        else true
    }
}