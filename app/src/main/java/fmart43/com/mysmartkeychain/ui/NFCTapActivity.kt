package fmart43.com.mysmartkeychain.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.hardware.fingerprint.FingerprintManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.databinding.ActivityNfctapBinding
import fmart43.com.mysmartkeychain.injection.ViewModelFactory
import fmart43.com.mysmartkeychain.ui.handler.FingerprintHandler
import fmart43.com.mysmartkeychain.ui.listener.IAuthenticateListener
import fmart43.com.mysmartkeychain.ui.viewmodel.NFCTapViewModel
import kotlinx.android.synthetic.main.activity_nfctap.*


class NFCTapActivity : AppCompatActivity(), IAuthenticateListener {

    private lateinit var binding: ActivityNfctapBinding

    private lateinit var viewModel: NFCTapViewModel

    private var mFingerprintHandler: FingerprintHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_nfctap)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(NFCTapViewModel::class.java)

        binding.viewModel = viewModel

        initializeNFCInformation(intent)

        viewModel.authorizationStatus.observe(this, Observer {
            if (it != null) showResult(it, viewModel.errorMessage.value) else null
        })

        viewModel.fingerprintPassEnabled.observe(this, Observer {
            val cryptoObject = viewModel.findCryptoObjectForFingerprint()
            if (cryptoObject != null && it == View.VISIBLE) {
                Toast.makeText(this, "Use fingerprint to login", Toast.LENGTH_LONG).show()
                val fingerprintManager = getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
                mFingerprintHandler = FingerprintHandler(this, viewModel.fingerprintPass!!, this)
                mFingerprintHandler!!.startAuth(fingerprintManager, cryptoObject)
            }
        })

        btn_access.setOnClickListener {
            viewModel.accessClick()
        }
    }

    private fun initializeNFCInformation(intent: Intent) {
        val nfcCode: String = intent.data.path
        val nfcCodeArray: List<String> = nfcCode.split("/")
        if (nfcCodeArray.size > 3) {
            viewModel.initialize(nfcCodeArray.get(3), nfcCodeArray.get(4))
        }
    }

    private fun showResult(authorizationStatus: Boolean, messageErrorResource: Int?) {
        val intent = Intent(this, AccessResultActivity::class.java)
        val result =
                if (authorizationStatus) {
                    "ACCESS AUTHORIZED"
                } else {
                    "ACCESS DENIED"
                }
        intent.putExtra("result", result)
        intent.putExtra("errorMessage", if (messageErrorResource != null) getString(messageErrorResource) else null)
        startActivity(intent)
    }

    override fun onAuthenticate(decryptPassword: String) {
        viewModel.password.value = decryptPassword
        viewModel.accessClick()
    }
}
