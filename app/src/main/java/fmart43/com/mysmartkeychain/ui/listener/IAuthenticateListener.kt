package fmart43.com.mysmartkeychain.ui.listener

interface IAuthenticateListener {

    fun onAuthenticate(decryptPassword: String)
}

