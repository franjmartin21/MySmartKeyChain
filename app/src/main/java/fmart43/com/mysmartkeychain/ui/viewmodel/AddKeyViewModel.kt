package fmart43.com.mysmartkeychain.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.net.Uri
import android.support.design.widget.Snackbar
import android.view.View
import fmart43.com.mysmartkeychain.R
import fmart43.com.mysmartkeychain.exception.KeyException
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.network.dto.KeyProfileDTO
import fmart43.com.mysmartkeychain.network.dto.StartRegistrationResponseDTO
import fmart43.com.mysmartkeychain.repository.KeyRepository
import fmart43.com.mysmartkeychain.ui.viewmodel.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.sql.Timestamp


class AddKeyViewModel(val keyRepository: KeyRepository) : BaseViewModel() {

    var urlText: MutableLiveData<String> = MutableLiveData()

    val keyReturned: MutableLiveData<Key> = MutableLiveData()

    var subscription: Disposable? = null

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val displayStep1: MutableLiveData<Int> = MutableLiveData()

    val displayStep2: MutableLiveData<Int> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()

    /**
     * Copied from step 2
     */

    val url: MutableLiveData<String> = MutableLiveData()

    val nfc: MutableLiveData<String> = MutableLiveData()

    val organization: MutableLiveData<String> = MutableLiveData()

    val userName: MutableLiveData<String> = MutableLiveData()

    val password: MutableLiveData<String> = MutableLiveData()

    val fingerprintAvailable: MutableLiveData<Boolean> = MutableLiveData()

    val fingerprintAuthenticationEnabled: MutableLiveData<Boolean> = MutableLiveData()

    val firstName: MutableLiveData<String> = MutableLiveData()

    val lastName: MutableLiveData<String> = MutableLiveData()

    val registrationCompleted: MutableLiveData<Boolean> = MutableLiveData()

    init {
        displayStep1.value = View.VISIBLE
        displayStep2.value = View.GONE
        loadingVisibility.value = View.GONE
        fingerprintAuthenticationEnabled.value = false
        fingerprintAvailable.value = keyRepository.isFingerprintAvailable()
        urlText.value = "http://"
    }

    fun nextStep(view: View) {
        val url = Uri.parse(urlText.value?:"")
        if (!isValidUrl(url)) {
            Snackbar.make(view, view.context.getString(R.string.incorrect_format_url_message), Snackbar.LENGTH_LONG).show()
        } else {
            subscription = Observable.fromCallable { keyRepository.startRegistration(urlText.value.toString()) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { onCallStartRegistrationStart() }
                    .doOnTerminate { onCallStartRegistrationFinished() }
                    .subscribe(
                            { result -> onCallStartRegistrationSuccess(result) },
                            { error -> onCallStartRegistrationError(error) }

                    )
        }
    }

    private fun onCallStartRegistrationStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onCallStartRegistrationFinished() {
        loadingVisibility.value = View.GONE

    }

    private fun onCallStartRegistrationSuccess(startRegistrationResponseDto: StartRegistrationResponseDTO) {
        keyReturned.value = null
        organization.value = startRegistrationResponseDto.nameCompany
        url.value = urlText.value
        nfc.value = startRegistrationResponseDto.nfcCode

        displayStep1.value = View.GONE
        displayStep2.value = View.VISIBLE
    }

    private fun onCallStartRegistrationError(e: Throwable) {
        if(e is KeyException)
            errorMessage.value = e.resourceStrId
    }


    /**
     * Step 2 copied
     */
    fun onRegister(view: View) {

        if (validateDataRegistration()) {

            val key: Key = Key(null, Timestamp(System.currentTimeMillis()), organization.value.toString(), url.value.toString(), nfc.value.toString(), userName.value.toString())

            val keyProfile = KeyProfileDTO(
                    userName.value.toString(),
                    password.value.toString(),
                    fingerprintAuthenticationEnabled.value?:false,
                    firstName.value.toString(),
                    lastName.value.toString())

            subscription = Observable.fromCallable {
                keyRepository.completeRegistration(url.value.toString(), keyProfile, key)
            }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { onCallCompleteRegistrationStart() }
                    .doOnTerminate { onCallCompleteRegistrationFinished() }
                    .subscribe(
                            { result -> onCallCompleteRegistrationSuccess() },
                            { error -> onCallCompleteRegistrationError(error) }

                    )
        }
        /*
        val intent = Intent(view.context, AddKeyStep2Activity::class.java)
        view.context.startActivity(intent)
        */

    }

    private fun validateDataRegistration(): Boolean {
        return if (userName.value.isNullOrBlank()) {
            errorMessage.value = R.string.username_empty_error
            false
        } else if (firstName.value.isNullOrBlank()) {
            errorMessage.value = R.string.firstname_empty_error
            false
        } else if (lastName.value.isNullOrBlank()) {
            errorMessage.value = R.string.lastname_empty_error
            false
        } else if (password.value.isNullOrBlank()) {
            errorMessage.value = R.string.password_empty_error
            false
        } else {
            true
        }
    }


    private fun onCallCompleteRegistrationStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onCallCompleteRegistrationFinished() {
        loadingVisibility.value = View.GONE
    }

    private fun onCallCompleteRegistrationSuccess() {
        registrationCompleted.value = true
    }

    private fun onCallCompleteRegistrationError(e: Throwable) {
        if(e is KeyException)
            errorMessage.value = e.resourceStrId
        else
            errorMessage.value = R.string.complete_registration_error
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }

}

