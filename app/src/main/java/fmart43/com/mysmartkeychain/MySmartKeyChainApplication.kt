package fmart43.com.mysmartkeychain

import android.app.Application
import timber.log.Timber

class MySmartKeyChainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {

        }
    }
}