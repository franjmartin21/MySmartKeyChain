package fmart43.com.mysmartkeychain.model.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import fmart43.com.mysmartkeychain.model.Key

@Dao
interface KeyDao {

    @Query("SELECT * FROM Key")
    fun getAll(): List<Key>

    @Query("SELECT * FROM Key where nfc = :nfc")
    fun getByNfc(nfc: String): Key?

    /*
        @get:Query("SELECT * FROM Key")
        val all: List<Key>
    */
    @Insert
    fun insert(key: Key)

}