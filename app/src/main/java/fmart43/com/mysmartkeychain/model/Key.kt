package fmart43.com.mysmartkeychain.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "key", indices = arrayOf(Index("nfc", unique = true)))
data class Key(
        @field:PrimaryKey(autoGenerate = true) val id: Int?,
        val timestamp: Date,
        val organization: String,
        val url: String,
        val nfc: String,
        val userName: String,
        var fingerprintPass: String? = null
)