package fmart43.com.mysmartkeychain.model.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.model.dao.KeyDao
import fmart43.com.mysmartkeychain.utils.ioThread
import java.sql.Timestamp

@Database(entities = arrayOf(Key::class), version = 1)
@TypeConverters(DateConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun keyDao(): KeyDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "MySmartKeyChain.db")
                        // prepopulate the database after onCreate was called
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                // insert the data on the IO Thread
                                ioThread {
                                    //getInstance(context).keyDao().insert(KEY_INSERT)
                                }

                            }
                        })
                        .build()
    }
}