package fmart43.com.mysmartkeychain.model.database

import android.arch.persistence.room.TypeConverter
import java.util.*

class DateConverters {
    @TypeConverter
    fun dateToLong(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun longToDate(time: Long?): Date? {
        return if (time == null) null else Date(time)
    }
}