package fmart43.com.mysmartkeychain.network

import fmart43.com.mysmartkeychain.network.dto.*
import retrofit2.Call
import retrofit2.mock.Calls

class KeyApiMock : KeyApi {

    val NFC_CODE: String = "NT1"

    val USERNAME: String = "hellohello@gmail.com"

    val COMPANY_NAME: String = "Facility Company"

    val DOOR_NAME: String = "Kitche's Door"

    override fun startRegistration(): Call<StartRegistrationResponseDTO> {
        val startRegistrationResponseDto = StartRegistrationResponseDTO(COMPANY_NAME, NFC_CODE)
        return Calls.response(startRegistrationResponseDto)
    }

    override fun completeRegistration(keyProfile: KeyProfileDTO):Call<Void> {
        return Calls.response(null)
    }

    override fun initializeUserDoorAuthorization(authorizationRequestDto: AuthorizationRequestDTO): Call<InitializeUserDoorAuthorizationResponseDTO> {
        val response = InitializeUserDoorAuthorizationResponseDTO(COMPANY_NAME, DOOR_NAME, USERNAME)
        return Calls.response(response)
    }

    override fun completeDoorAuthorization(authorizationRequestDto: AuthorizationRequestDTO): Call<Void> {
        return Calls.response(null)
    }

}
