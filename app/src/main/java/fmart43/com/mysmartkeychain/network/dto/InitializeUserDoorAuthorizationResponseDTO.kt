package fmart43.com.mysmartkeychain.network.dto

import com.squareup.moshi.Json
import java.net.PasswordAuthentication

data class InitializeUserDoorAuthorizationResponseDTO(@field:Json(name = "nameCompany") val nameCompany: String,
                                                      @field:Json(name = "doorName") val doorName: String,
                                                      @field:Json(name = "userName") val userName: String,
                                                      var fingerprintPass: String? = null
)