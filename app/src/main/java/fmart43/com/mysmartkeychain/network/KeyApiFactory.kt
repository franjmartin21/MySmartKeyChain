package fmart43.com.mysmartkeychain.network

import fmart43.com.mysmartkeychain.BuildConfig
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object KeyApiFactory {

    fun createKeyApi(url: String): KeyApi {
        return when (BuildConfig.BUILD_TYPE) {
            "debug" -> KeyApiMock()
            else -> {
                val okHttpClientBuilder = OkHttpClient.Builder()
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                okHttpClientBuilder.addInterceptor(logging)

                Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .client(okHttpClientBuilder.build())
                    .build().create(KeyApi::class.java)
            }
        }
    }
}