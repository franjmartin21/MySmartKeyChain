package fmart43.com.mysmartkeychain.network.dto

import com.squareup.moshi.Json

data class StartRegistrationResponseDTO(@field:Json(name = "nameCompany")val nameCompany: String?,
                                        @field:Json(name = "nfcCode")val nfcCode: String?)