package fmart43.com.mysmartkeychain.network.dto

import com.squareup.moshi.Json

data class KeyProfileDTO(
        @field:Json(name = "username") val username: String,
        @field:Json(name = "password") val password: String,
        @field:Json(name = "fingerprintEnabled") val fingerprintEnabled: Boolean,
        @field:Json(name = "firstName") val firstName: String,
        @field:Json(name = "lastName") val lastName: String
)