package fmart43.com.mysmartkeychain.network

import fmart43.com.mysmartkeychain.network.dto.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface KeyApi {

    @GET("/api/v1/registration/start")
    fun startRegistration(): Call<StartRegistrationResponseDTO>

    @POST("/api/v1/registration/complete")
    fun completeRegistration(@Body keyProfileDTO: KeyProfileDTO): Call<Void>

    @POST("/api/v1/authorization/check")
    fun initializeUserDoorAuthorization(@Body authorizationRequestDTO: AuthorizationRequestDTO): Call<InitializeUserDoorAuthorizationResponseDTO>

    @POST("/api/v1/authorization/authorize")
    fun completeDoorAuthorization(@Body authorizationRequestDTO: AuthorizationRequestDTO): Call<Void>

}