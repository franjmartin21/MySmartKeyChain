package fmart43.com.mysmartkeychain.network.dto

import com.squareup.moshi.Json

data class AuthorizationRequestDTO(@field:Json(name = "employeeUsername") val userName: String,
                                                     @field:Json(name = "doorNfcCode") val doorId: String,
                                                     @field:Json(name = "employeePassword") var password: String? = null)