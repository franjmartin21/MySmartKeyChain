package fmart43.com.mysmartkeychain.repository

import android.hardware.fingerprint.FingerprintManager
import android.os.SystemClock
import fmart43.com.mysmartkeychain.exception.KeyException
import fmart43.com.mysmartkeychain.fingerprint.FingerprintAPI
import fmart43.com.mysmartkeychain.model.Key
import fmart43.com.mysmartkeychain.model.dao.KeyDao
import fmart43.com.mysmartkeychain.network.KeyApiFactory
import fmart43.com.mysmartkeychain.network.dto.*
import timber.log.Timber

class KeyRepository private constructor(val keyDao: KeyDao, val keyApiFactory: KeyApiFactory, val fingerprintAPI: FingerprintAPI) {

    companion object {
        private var INSTANCE: KeyRepository? = null

        fun getInstance(keyDao: KeyDao, keyApiFactory: KeyApiFactory, fingerprintAPI: FingerprintAPI): KeyRepository =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: KeyRepository(keyDao, keyApiFactory, fingerprintAPI).also { INSTANCE = it }
                }
    }


    fun getAllRegisteredKeys(): List<Key> {
        val keyList: List<Key> = keyDao.getAll()
        SystemClock.sleep(1000)
        return keyList
    }

    fun startRegistration(url: String): StartRegistrationResponseDTO {
        val keyApi = keyApiFactory.createKeyApi(url)
        val call = keyApi.startRegistration()
        return try {
            val response = call.execute()
            return if (response?.code() == 200)
                response?.body() ?: throw KeyException.UNKNOWN_PROBLEM_SERVER
            else
                throw KeyException.SERVER_NOT_FOUND
        } catch (e: Exception){
            Timber.e(e.message)
            if(e is KeyException) throw e else throw KeyException.SERVER_NOT_FOUND
        }
    }

    fun completeRegistration(url: String, keyProfileDTO: KeyProfileDTO, key: Key){
        val keyApi = keyApiFactory.createKeyApi(url)
        try {
            if(keyProfileDTO.fingerprintEnabled)
                key.fingerprintPass = generateFingerprintPass(keyProfileDTO.password)

            val call = keyApi.completeRegistration(keyProfileDTO)
            val response = call.execute()
            when(response?.code()){
                201 -> keyDao.insert(key)
                in 400..499-> throw KeyException.INCOMPLETE_DATA_REGISTRATION
                else -> KeyException.UNKNOWN_PROBLEM_SERVER
            }
        } catch (e: Exception){
            Timber.e(e.message)
            if(e is KeyException) throw e else throw KeyException.SERVER_NOT_FOUND
        }
    }

    private fun generateFingerprintPass(password: String): String? {
        return try{
            encryptString(password)
        } catch (e: Throwable){
            throw KeyException.FINGERPRING_REGISTRATION_PROBLEM
        }
    }

    fun initAuthorizationOnDoor(nfc: String, doorId: String): InitializeUserDoorAuthorizationResponseDTO? {
        val key: Key = keyDao.getByNfc(nfc) ?: throw KeyException.KEY_UNREGISTERED
        val keyApi = keyApiFactory.createKeyApi(key.url)

        return try {
            val call = keyApi.initializeUserDoorAuthorization(AuthorizationRequestDTO(key.userName, doorId))
            val response = call.execute()
            when(response?.code()){
                200 -> {
                    val response: InitializeUserDoorAuthorizationResponseDTO? = response.body()
                    response?.fingerprintPass = key.fingerprintPass
                    response
                }
                401 -> throw KeyException.UNATHORIZED_ERROR
                417 -> throw KeyException.UNREGISTERED_USER_IN_SERVER
                else -> throw KeyException.UNKNOWN_PROBLEM_SERVER
            }
        } catch (e: Exception){
            Timber.e(e.message)
            if(e is KeyException) throw e else throw KeyException.SERVER_NOT_FOUND
        }
    }

    fun completeAuthorizationOnDoor(nfc: String, doorId: String, password: String): Boolean {
        val key: Key = keyDao.getByNfc(nfc) ?: throw KeyException.KEY_UNREGISTERED
        val keyApi = keyApiFactory.createKeyApi(key.url)

        return try {
            val call = keyApi.completeDoorAuthorization(AuthorizationRequestDTO(key.userName, doorId, password))
            val response = call.execute()
            when(response?.code()){
                200 -> true
                400 -> throw KeyException.INCORRECT_PASSWORD
                401 -> throw KeyException.UNATHORIZED_ERROR
                417 -> throw KeyException.UNREGISTERED_USER_IN_SERVER
                else -> throw KeyException.UNKNOWN_PROBLEM_SERVER
            }
        } catch (e: Exception){
            Timber.e(e.message)
            if(e is KeyException) throw e else throw KeyException.SERVER_NOT_FOUND
        }
    }

    fun isFingerprintAvailable(): Boolean {
        return fingerprintAPI.checkForEnrolledFingerPrints() && fingerprintAPI.checkForSupportedHardware() && fingerprintAPI.checkForKeyguardSecured()
    }

    fun encryptString(str: String): String? {
        return fingerprintAPI.encryptString(str)
    }

    fun getCryptoObjectForFingerprint(): FingerprintManager.CryptoObject? {
        return fingerprintAPI.getCryptoObject()
    }
}