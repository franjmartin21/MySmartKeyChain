package fmart43.com.mysmartkeychain.exception

import fmart43.com.mysmartkeychain.R

class KeyException(override var message: String, val resourceStrId: Int) : Exception(message) {

    companion object {
        private val messageKeyException = "Problem in key repository"

        val KEY_UNREGISTERED = KeyException(messageKeyException, R.string.key_unregistered_error)
        val UNATHORIZED_ERROR = KeyException(messageKeyException, R.string.unauthorized_error)
        val UNREGISTERED_USER_IN_SERVER = KeyException(messageKeyException, R.string.unregistered_user_server)
        val INCORRECT_PASSWORD = KeyException(messageKeyException, R.string.incorrect_password_error)
        val INCORRECT_FINGERPRINT = KeyException(messageKeyException, R.string.incorrect_fingerprint_error)
        val UNKNOWN_PROBLEM_SERVER = KeyException(messageKeyException, R.string.unknown_server_error)
        val SERVER_NOT_FOUND = KeyException(messageKeyException, R.string.server_not_found)
        val INCOMPLETE_DATA_REGISTRATION = KeyException(messageKeyException, R.string.incomplete_data)

        val FINGERPRING_REGISTRATION_PROBLEM = KeyException(messageKeyException, R.string.fingerpring_registration_error)
    }

}