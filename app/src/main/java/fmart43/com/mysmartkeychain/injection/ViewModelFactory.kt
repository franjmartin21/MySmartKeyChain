package fmart43.com.mysmartkeychain.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v7.app.AppCompatActivity
import fmart43.com.mysmartkeychain.fingerprint.FingerprintAPI
import fmart43.com.mysmartkeychain.model.database.AppDatabase
import fmart43.com.mysmartkeychain.network.KeyApiFactory
import fmart43.com.mysmartkeychain.repository.KeyRepository
import fmart43.com.mysmartkeychain.ui.viewmodel.AddKeyViewModel
import fmart43.com.mysmartkeychain.ui.viewmodel.KeyListViewModel
import fmart43.com.mysmartkeychain.ui.viewmodel.NFCTapViewModel

class ViewModelFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val db = AppDatabase.getInstance(activity)
        val fingerprintAPI = FingerprintAPI.getInstance(activity)
        val keyRepository: KeyRepository = KeyRepository.getInstance(db.keyDao(), KeyApiFactory, fingerprintAPI)

        return when {
            modelClass.isAssignableFrom(KeyListViewModel::class.java) -> @Suppress("UNCHECKED_CAST")
            KeyListViewModel(keyRepository) as T
            modelClass.isAssignableFrom(AddKeyViewModel::class.java) -> @Suppress("UNCHECKED_CAST")
            AddKeyViewModel(keyRepository) as T
            modelClass.isAssignableFrom(NFCTapViewModel::class.java) -> @Suppress("UNCHECKED_CAST")
            NFCTapViewModel(keyRepository) as T
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}